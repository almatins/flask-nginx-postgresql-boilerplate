import time
import os
import sys
import logging


def logIt(level, msg, is_exit=False):
    # Create a custom logger
    logger = logging.getLogger(__name__)

    # Create handlers
    c_handler = logging.StreamHandler()
    f_handler = logging.FileHandler('error.log')
    d_handler = logging.FileHandler('debug.log')

    c_handler.setLevel(logging.WARNING)
    f_handler.setLevel(logging.ERROR)
    d_handler.setLevel(logging.DEBUG)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    d_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    c_handler.setFormatter(c_format)
    f_handler.setFormatter(f_format)
    d_handler.setFormatter(d_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)

    if level == 'error':
        logger.error(msg)

    if level == 'warning':
        logger.warning(msg)

    if level == 'debug':
        logger.debug(msg)

    if is_exit:
        sys.exit(1)

    pass


def logMe(msg, status='error'):
    namafile = time.strftime('%Y%m%d')
    namafile = 'error_%s.log' % namafile

    if os.path.exists(namafile):
        # filenya ada
        logfile = open(namafile, 'a+')
    else:
        logfile = open(namafile, 'w+')

    timestamp = time.strftime('%d/%m/%Y %H:%M:%s')
    try:
        logfile.write('%s : %s \r\n' % (timestamp, msg))
    finally:
        logfile.close()


def logQuery(msg):
    namafile = time.strftime('%Y%m%d')
    namafile = 'update_%s.log' % namafile

    if os.path.exists(namafile):
        # filenya ada
        logfile = open(namafile, 'a+')
    else:
        logfile = open(namafile, 'w+')

    timestamp = time.strftime('%d/%m/%Y %H:%M:%s')
    try:
        logfile.write('%s : %s \r\n' % (timestamp, msg))
    finally:
        logfile.close()
